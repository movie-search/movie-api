import express from 'express'
import Api from './api'

const app = express()

app.get('/CommonSenseMedia', Api.CommonSenseMedia)
app.get('/RottenTomatoes', Api.RottenTomatoes)
app.get('/OMDb', Api.OMDb)
app.get('/Moowee', Api.Moowee)

app.listen(3000, () => {
  console.log('Listening on port 3000')
})
