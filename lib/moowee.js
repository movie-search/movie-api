import omdb from './omdb'
import csm from './csm'
import rt from './rt'

const toSlug = (title) => title.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-')

const search = async (title) => new Promise((resolve, reject) => {
  omdb(title)
    .then((omdbResults) => {
      const omdbResult = omdbResults[0]
      const movieTitle = omdbResult.title
      Promise.all([csm(movieTitle), rt(movieTitle)])
        .then(results => {
          const [csmResult, rtResult] = results
          resolve({
            omdb: omdbResult,
            csm: csmResult,
            rt: rtResult
          })
        })
        .catch(reject)
    })
    .catch(reject)
})

export default {
  search
}
